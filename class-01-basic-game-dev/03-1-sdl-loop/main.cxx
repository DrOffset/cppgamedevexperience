#include <algorithm>
#include <vector>
#include <cstdlib>
#include <iostream>
#include <string_view>

#include <SDL2/SDL.h>

#pragma pack(push, 4)
struct BindKeys {
    SDL_Keycode key;
    std::string_view name;
};
#pragma pack(pop)

void check_input(const SDL_Event& event)
{
    const std::vector<::BindKeys> keys { { { SDLK_UP, "UP" },
        { SDLK_LEFT, "LEFT" },
        { SDLK_DOWN, "DOWN" },
        { SDLK_RIGHT, "RIGHT" },
        { SDLK_a, "A" },
        { SDLK_b, "B" },
        { SDLK_SPACE, "SELECT" },
        { SDLK_RETURN, "START" } } };

    const auto iterator = std::find_if(keys.begin(), keys.end(), [&](const ::BindKeys& a) {
        return a.key == event.key.keysym.sym;
    });

    if (iterator != keys.end()) {
        std::cout << iterator->name;
        if (event.type == SDL_KEYDOWN) {
            std::cout << " was pressed!" << std::endl;
        } else {
            std::cout << " has been released!" << std::endl;
        }
    }
}

void showSDLVersion()
{
    SDL_version v = { 0, 0, 0 };

    SDL_GetVersion(&v);

    std::cout << "Installed SDL: " << static_cast<int>(v.major) << "."
              << static_cast<int>(v.minor) << "." << static_cast<int>(v.patch)
              << std::endl;
}

int main(int /*argc*/, char* /*argv*/[])
{
    showSDLVersion();


    int window_width = /*1515*/320;
    int window_height = /*525*/240;
    SDL_Init(SDL_INIT_VIDEO);

    SDL_Window* window = SDL_CreateWindow("NES Controller", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, window_width, window_height, 0);
    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    //SDL_Surface* image = SDL_LoadBMP("nescontroller.bmp");
    //SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, image);

    if (window == nullptr) {
        const char* err_mes = SDL_GetError();
        std::cerr << "Error! " << err_mes << std::endl;
        SDL_Quit();
        return EXIT_FAILURE;
    }

    /*bool quit = false;
    SDL_Event render;
    while (!quit) {
        SDL_WaitEvent(&render);

        switch (render.type) {
        case SDL_QUIT:
            quit = true;
            break;
        }

        SDL_RenderCopy(renderer, texture, nullptr, nullptr);
        SDL_RenderPresent(renderer);
    }*/

    bool game_loop = true;
    while (game_loop) {
        SDL_Event keystroke;
        while (SDL_PollEvent(&keystroke)) {
            switch (keystroke.type) {
            case SDL_KEYDOWN:
                check_input(keystroke);
                break;
            case SDL_KEYUP:
                check_input(keystroke);
                break;
            case SDL_QUIT:
                game_loop = false;
                break;
            default:
                break;
            }
        }
    }

    //SDL_DestroyTexture(texture);
    //SDL_FreeSurface(image);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);

    SDL_Quit();

    return EXIT_SUCCESS;
}
