#include <algorithm>
#include <array>
#include <cstdlib>
#include <iostream>
#include <memory>
#include <string_view>

#include "engine.hxx"

int main(int /*argc*/, char* /*argv*/[])
{
    std::unique_ptr<dedsec::engine, void (*)(dedsec::engine*)> engine(
        dedsec::create_engine(), dedsec::destroy_engine);

    std::string err = engine->initialize("");
    if (!err.empty()) {
        std::cerr << err << std::endl;
        return EXIT_FAILURE;
    }

    int count {};
    while (count != 2) {
        std::cout << "NES POWER ON" << std::endl;
        count++;
    }

    bool game_loop = true;
    while (game_loop) {
        dedsec::KeyEvent event;
        while (engine->read_input(event)) {
            std::cout << event << std::endl;
            switch (event) {
            case dedsec::KeyEvent::turn_off:
                std::cout << event << std::endl;
                game_loop = false;
                break;
            default:
                break;
            }
        }
    }

    engine->uninitialize();

    return EXIT_SUCCESS;
}
